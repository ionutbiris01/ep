﻿Player Prefs Plus - Read Me.

Before using Player Prefs Plus you must do the following

In the Unity Editor
Move the folder Assets/SSSoftware/SQL4Unity/StreamingAssets to Assets/
If your project already has an Assets/StreamingAssets folder move the contents of SSSoftware/SQL4Unity/StreamingAssets to Assets/StreamingAssets

To access the Player Prefs Plus help documentation use

Tools->Player Prefs Plus->Help

SteveSmith.SoftWare 2018.

Release Notes:

	Februrary 2020:
		Upgraded to latest version of SQL4Unity.
		Added 2 new methods to PlayerPrefsPlus and PlayerPrefsSQL
			GetAllPlayers - Retrieve all player details
			GetPlayerCount - Retrieve the number of players

