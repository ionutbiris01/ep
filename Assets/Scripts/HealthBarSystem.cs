using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarSystem : MonoBehaviour
{
    //public Text healthText;
   public Image HealthBar;
    public float currenthealth, maxhealth = 100f;
    float lerpspeed;
    // Start is called before the first frame update
    void Start()
    {
        currenthealth = maxhealth;
        lerpspeed = 3f * Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        //healthText.text = "Health: " + currenthealth + "%";
        if(currenthealth>maxhealth)
        {
            currenthealth = maxhealth;
        }
        ChangeHealthColor();
    }
    void HealthBarFiller()
    {
        HealthBar.fillAmount = Mathf.Lerp(HealthBar.fillAmount, currenthealth / maxhealth, lerpspeed);
    }
    void ChangeHealthColor()
    {
        Color healthcolor = Color.Lerp(Color.red, Color.green, (currenthealth / maxhealth));
        HealthBar.color = healthcolor;
    }
    public void Damage(float DamagePoint)
    {
        if (currenthealth > 0)
            currenthealth -= DamagePoint;
    }
    public void Heal ( float HealingPoints)
    {
        if (currenthealth < 100 && currenthealth > 0)
            currenthealth += HealingPoints;
    }
}
