using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObject cameraRoot;

    [SerializeField] private Cinemachine.CinemachineVirtualCamera virtualCamera;

    [SerializeField] GameObject mainCamera;

    private Vector3 thirdPersonCameraRootPos = new Vector3(.5f, 1.375f, -4f);
    private Vector3 firstPersonCameraRootPos = new Vector3(0f, 1.65f, 0.292f);

    private Vector3 thirdPersonMainCameraPos = new Vector3(0f, 2.14f, -3.7f);
    private Vector3 firstPersonMainCameraPos = new Vector3(0f, 2.14f, .292f);

    [SerializeField] Transform thirdview;
    [SerializeField] Transform firstview;


    public GameObject fstperson;
    public GameObject trdperson;

    public bool isFirstPerson = true;

    public void ChangeCamera()
    {
        if (!isFirstPerson)
        {
            /*
            cameraRoot.transform.position = firstPersonCameraRootPos;
            cameraRoot.transform.rotation = Quaternion.identity;

            mainCamera.transform.position = firstPersonMainCameraPos;
            

            virtualCamera.m_Follow = firstview;

            CinemachineComponentBase componentBase = virtualCamera.GetCinemachineComponent(CinemachineCore.Stage.Body);
            (componentBase as Cinemachine3rdPersonFollow).CameraDistance = 0;
            (componentBase as Cinemachine3rdPersonFollow).Damping = new Vector3(.1f, .25f, .3f);
            */
            fstperson.SetActive(true);
            fstperson.transform.position = trdperson.transform.position;
            fstperson.transform.rotation = trdperson.transform.rotation;
            trdperson.SetActive(false);
            

            isFirstPerson = true;
        }
        else
        {
            /*
            cameraRoot.transform.position = firstPersonCameraRootPos;
            cameraRoot.transform.rotation = Quaternion.identity;

            mainCamera.transform.position = firstPersonMainCameraPos;
            

            virtualCamera.m_Follow = thirdview;
            CinemachineComponentBase componentBase = virtualCamera.GetCinemachineComponent(CinemachineCore.Stage.Body);
            (componentBase as Cinemachine3rdPersonFollow).CameraDistance = 4;
            (componentBase as Cinemachine3rdPersonFollow).Damping = new Vector3(.1f, .1f, .3f);
            */
            trdperson.SetActive(true);
            trdperson.transform.position = fstperson.transform.position;
            trdperson.transform.rotation = fstperson.transform.rotation;
            fstperson.SetActive(false);

            isFirstPerson = false;
        }
    }
}
