using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    [Space(10)]
    [Header("Wolf Blood")]
    public int wolfBloodCounter;
    [SerializeField] private TMP_Text wolfBloodText;
    [SerializeField] private TMP_Text messageToPlayer;

    [Space(10)]
    [Header("Character Views")]
    [SerializeField] CameraController cameraController;
    public bool isFirstPerson;

    [Space(10)]
    [Header("Character Controllers")]
    [SerializeField] private StarterAssets.ThirdPersonController thirdPersonController; 
    [SerializeField] private StarterAssets.FirstPersonController firstPersonController;
    [SerializeField] private int dashSpeed = 40;

    private float normalSpeed;
    void Start()
    {
        normalSpeed = thirdPersonController.MoveSpeed;
        UpdateWolfBloodUI();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.V)) // change camera
        {
            cameraController.ChangeCamera();
        }
        Dash();
    }
    private void Dash()
    {
        //When holding Q on keyboard dash is activated. 
        //

        if (Input.GetKeyDown(KeyCode.Q)) // activate dash
        {
            if (isFirstPerson)
            {
                firstPersonController.MoveSpeed = dashSpeed;
            }
            else
            {
                thirdPersonController.MoveSpeed = dashSpeed;
            }
        }
        if (Input.GetKeyUp(KeyCode.Q)) // deactivate dash
        {
            if (isFirstPerson)
            {
                firstPersonController.MoveSpeed = normalSpeed;
            }
            else
            {
                thirdPersonController.MoveSpeed = normalSpeed;
            }
        }
    }
    public void UpdateWolfBloodUI()
    {
        wolfBloodText.text = "Wolf Blood Count: " + wolfBloodCounter.ToString();
    }
    public void UIMessageToPlayer(string message)
    {
        messageToPlayer.text = message;
        messageToPlayer.gameObject.SetActive(true);

        Invoke(nameof(DeactivateUIMessageToPlayer), 3f);
    }
    public void DeactivateUIMessageToPlayer()
    {
        messageToPlayer.text = "";
        messageToPlayer.gameObject.SetActive(false);
    }
    public void AddWolfBlood()
    {
        wolfBloodCounter += 1;
        UpdateWolfBloodUI();
        UIMessageToPlayer("Wolf Blood Collected!");
    }


}
