using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    enum ItemType { WolfBlood}

    [SerializeField] private ItemType itemType;
    private Player player;

    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(itemType == ItemType.WolfBlood)
        {
            player.AddWolfBlood();
            Destroy(this.gameObject);
        }
    }
}
