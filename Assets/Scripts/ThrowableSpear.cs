using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowableSpear : MonoBehaviour
{
    public Rigidbody Spear;
    public float throwforce = 50f;

    private void Start()
    {
     
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(1))
        {
           
            ThrowSpear();
        }
        
    }
    //Throw Spear
    void ThrowSpear()
    {
        Spear.transform.parent = null;
        Spear.isKinematic = false;
        Spear.AddForce(Camera.main.transform.TransformDirection(Vector3.forward)*throwforce, ForceMode.Impulse);
    }
}
